function renderPR(prArr) {
  var prGrid = document.querySelector("#prGrid");
  var contentHTML = "";
  for (var i = 0; i < prArr.length; i++) {
    var pr = prArr[i];
    var contentTr = `<div class="product__item p-3 sm:p-4">
      <div class="product__img">
        <img src="${pr.img}" alt="product1">
      </div>
      <div class="product__info mt-4">
        <div class="product__name">
          <h3 class="text-base font-semibold">${pr.name}</h3>
        </div>
        <div class="product__price">
          <p class="text-lg text-green-500">${parseFloat(
            pr.price
          ).toLocaleString("en-US", { style: "currency", currency: "USD" })}</p>
        </div>
      </div>
      <div class="product__detailTbl mt-2">
        <table class="table table-striped">
          <tbody>
            <tr>
              <th scope="row"><i class="fa-solid fa-mobile"></i> Screen</th>
              <td>${pr.screen}</td>
            </tr>
            <tr>
              <th scope="row"><i class="fa-solid fa-camera"></i> Front camera</th>
              <td>${pr.backCamera}</td>
            </tr>
            <tr>
              <th scope="row"><i class="fa-solid fa-camera-retro"></i> Back camera</th>
              <td>${pr.frontCamera}</td>
            </tr>
            <tr>
              <th scope="row"><i class="fa-solid fa-info-circle"></i> Description</th>
              <td>${pr.desc}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="product__actions mt-2">
        <button data-id="${pr.id}" data-name="${pr.name}" data-price="${
      pr.price
    }" class="atcBtn w-100 bg-blue-500 hover:bg-blue-700 text-white text-xs font-semibold py-2 px-4 rounded"><i class="fa-solid fa-shopping-cart"></i> Add to cart</button>
      </div>
    </div>`;
    contentHTML += contentTr;
  }
  prGrid.innerHTML = contentHTML;
  addClickEvent();
}

function renderPRType(prArr) {
  const productType = document.getElementById("productType");
  const typeSet = new Set(prArr.map(pr => pr.type));
  typeSet.forEach(type => {
    const option = document.createElement("option");
    option.innerHTML = type;
    productType.appendChild(option);
  });
}

function openLoading() {
  document.getElementById("loading").style.display = "grid";
}

function closeLoading() {
  document.getElementById("loading").style.display = "none";
}

function addClickEvent() {
  var addToCartButtons = document.getElementsByClassName("atcBtn");
  for (var i = 0; i < addToCartButtons.length; i++) {
    addToCartButtons[i].addEventListener("click", addToCart);
  }
}

function addToCart() {
  var prID = this.getAttribute("data-id");
  var prName = this.getAttribute("data-name");
  var prPrice = this.getAttribute("data-price");

  var cartItem = null;
  for (var j = 0; j < cart.length; j++) {
    if (cart[j].prID === prID) {
      cartItem = cart[j];
      break;
    }
  }

  if (cartItem !== null) {
    cartItem.prQuantity++;
    Toastify({
      text: "Add to cart successfully",
      offset: {
        x: 50,
        y: 60,
      },
    }).showToast();
  } else {
    cartItem = {
      prID: prID,
      prName: prName,
      prPrice: prPrice,
      prQuantity: 1,
    };
    cart.push(cartItem);
    Toastify({
      text: "Add to cart successfully",
      offset: {
        x: 50,
        y: 60,
      },
    }).showToast();
  }
  var dataJson = JSON.stringify(cart);
  localStorage.setItem("CART_LOCAL", dataJson);
  updateCart(cart);
}

function updateCart(cartArr) {
  var emptyCart = document.getElementById("emptyCart");
  var cartTable = document.getElementById("cartTbl");
  var totalQuantityBox = document.getElementById("totalQuantityBox");

  if (cartArr.length != 0) {
    emptyCart.style.display = "none";
    cartTable.innerHTML = "";

    var totalQuantity = 0;
    var totalPrice = 0;
    for (var i = 0; i < cartArr.length; i++) {
      totalPrice += cartArr[i].prPrice * cartArr[i].prQuantity;
      totalQuantity += cartArr[i].prQuantity;
    }
    totalQuantityBox.innerHTML = totalQuantity;
    totalQuantityBox.style.display = "inline-flex";

    var headerRow = document.createElement("tr");
    headerRow.classList = "bg-gray-100";
    headerRow.innerHTML = `<th class="w-5/12">Product</th>
      <th class="w-2/12">Price</th>
      <th class="w-2/12">Quantity</th>
      <th class="w-2/12">Amount</th>
      <th class="w-1/12"></th>`;
    cartTable.appendChild(headerRow);
    for (var j = 0; j < cartArr.length; j++) {
      var cartItem = cartArr[j];
      var row = document.createElement("tr");
      row.classList = "border border-slate-600";
      row.innerHTML = `<td>${cartItem.prName}</td>
      <td>${parseFloat(cartItem.prPrice).toLocaleString("en-US", {
        style: "currency",
        currency: "USD",
      })}</td>
      <td>
        <div class="number-input">
          <button onclick="this.parentNode.querySelector('input[type=number]').stepDown(),changeQuantity(${
            cartItem.prID
          },event)" ></button>
          <input onchange="changeQuantity(${
            cartItem.prID
          },event)" class="quantity" min="0" name="quantity" value="${
        cartItem.prQuantity
      }" type="number">
          <button onclick="this.parentNode.querySelector('input[type=number]').stepUp(),changeQuantity(${
            cartItem.prID
          },event)" class="plus"></button>
        </div>
      </td>
      <td>${parseFloat(cartItem.prPrice * cartItem.prQuantity).toLocaleString(
        "en-US",
        { style: "currency", currency: "USD" }
      )}</td>
      <td>
        <button onclick="removeFromCart(${
          cartItem.prID
        })" class="btn text-sm text-red-500 hover:text-red-700"><i class="fa fa-trash-alt"></i></button>
      </td>`;
      cartTable.appendChild(row);
    }

    var footerRow = document.createElement("tr");
    footerRow.innerHTML = `
    <td colspan="2"></td>
    <td><strong>Total</strong></td>
    <td colspan="2" class="font-semibold text-green-500">${totalPrice.toLocaleString(
      "en-US",
      { style: "currency", currency: "USD" }
    )}</td>`;
    cartTable.appendChild(footerRow);

    var actionRow = document.createElement("tr");
    actionRow.innerHTML = `<td colspan="5" class="text-right"><button onclick="clearCart()" class="clearCartBtn btn bg-red-500 hover:bg-red-700 text-white text-sm sm:text-base mx-2">Clear cart</button><button onclick="checkOutCart()" class="payBtn btn bg-blue-500 hover:bg-blue-700 text-white text-sm sm:text-base">Check out</button></td>`;
    cartTable.appendChild(actionRow);
  } else {
    cartTable.innerHTML = "";
    emptyCart.style.display = "block";
    totalQuantityBox.innerHTML = "0";
    totalQuantityBox.style.display = "none";
  }
}

function removeFromCart(id) {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].prID == id) {
      cart.splice(i, 1);
    }
  }
  var dataJson = JSON.stringify(cart);
  localStorage.setItem("CART_LOCAL", dataJson);
  updateCart(cart);
}

function changeQuantity(id, e) {
  if (e.target.parentNode.querySelector("input[type=number]").value > 0) {
    for (let i = 0; i < cart.length; i++) {
      if (cart[i].prID == id) {
        cart[i].prQuantity = Number(
          e.target.parentNode.querySelector("input[type=number]").value
        );
      }
    }
    var dataJson = JSON.stringify(cart);
    localStorage.setItem("CART_LOCAL", dataJson);
    updateCart(cart);
  } else removeFromCart(id);
}

function clearCart() {
  cart.splice(0, cart.length);
  Toastify({
    text: "Clear cart successfully",
    offset: {
      x: 50,
      y: 60,
    },
  }).showToast();
  var dataJson = JSON.stringify(cart);
  localStorage.setItem("CART_LOCAL", dataJson);
  updateCart(cart);
}

function checkOutCart() {
  cart.splice(0, cart.length);
  Toastify({
    text: "Your order is complete",
    offset: {
      x: 50,
      y: 60,
    },
  }).showToast();
  var dataJson = JSON.stringify(cart);
  localStorage.setItem("CART_LOCAL", dataJson);
  updateCart(cart);
}
