var cart = [];

var dataJson = localStorage.getItem("CART_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var dataItem = dataArr[i];
    var cartItem = {
      prID: dataItem.prID,
      prName: dataItem.prName,
      prPrice: dataItem.prPrice,
      prQuantity: dataItem.prQuantity,
    };
    cart.push(cartItem);
    updateCart(cart);
  }
}

const BASE_URL = "https://64463059ee791e1e29f88c78.mockapi.io/Products";

function fetchPR() {
  openLoading();
  prService
    .getList()
    .then(function (res) {
      renderPR(res.data);
      renderPRType(res.data);
      closeLoading();
    })
    .catch(function (err) {
      console.log(err);
      closeLoading();
    });
}
fetchPR();

document.getElementById("productType").addEventListener("change", function () {
  filterPR(this.value);
});

async function filterPR(prType) {
  openLoading();
  let prList = await axios.get(BASE_URL).then(function (res) {
    return res.data;
  });
  var filteredList = prList.filter(function (item) {
    return item.type.includes(prType);
  });
  if (filteredList != "") {
    renderPR(filteredList);
  } else renderPR(prList);
  closeLoading();
}
