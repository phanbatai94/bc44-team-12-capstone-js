function takeInfoFromForm() {
  var id = document.getElementById("txtPrID").value;
  var price = document.getElementById("txtPrice").value;
  var name = document.getElementById("txtName").value;
  var screen = document.getElementById("txtScreen").value;
  var backCamera = document.getElementById("txtBlCamera").value;
  var frontCamera = document.getElementById("txtFrCamera").value;
  var img = document.getElementById("txtImg").value;
  var desc = document.getElementById("txtDesc").value;
  var type = document.getElementById("txtType").value;

  var pr = new Product(
    id,
    price,
    name,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
  console.log(pr);
  return pr;
}

function renderProduct(pr) {
  var content = "";
  for (var i = 0; i < pr.length; i++) {
    contentEachOfPr = `
  <tr>
  <td>${pr[i].id}</td>
  <td>${pr[i].name}</td>
  <td>${pr[i].price}</td>
  <td>${pr[i].type}</td>
  <td>
  <button onclick="deletePr(${
    pr[i].id
  })" class="btn btn-danger"><i class="fa fa-trash"></i></button>
  <button data-toggle="modal"
  data-target="#myModal" onclick="editPr(${
    pr[i].id
  })" class="btn btn-success"><i class="fa fa-edit"></i></button>
  </td>
  
  </tr>
  `;
    content += contentEachOfPr;
  }
  document.getElementById("tbodyProduct").innerHTML = content;
}

// show info to form
function showInfoToForm(pr) {
  document.getElementById("txtPrID").value = pr.id;
  document.getElementById("txtPrice").value = pr.price;
  document.getElementById("txtName").value = pr.name;
  document.getElementById("txtScreen").value = pr.screen;
  document.getElementById("txtBlCamera").value = pr.backCamera;
  document.getElementById("txtFrCamera").value = pr.frontCamera;
  document.getElementById("txtImg").value = pr.img;
  document.getElementById("txtDesc").value = pr.desc;
  document.getElementById("txtType").value = pr.type;
}
// turn on and off Loading

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}

function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
