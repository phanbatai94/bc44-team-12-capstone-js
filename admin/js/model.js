function Product(
  _id,
  _price,
  _name,
  _screen,
  _backCamera,
  _frontCamera,
  _img,
  _desc,
  _type
) {
  this.id = _id;
  this.price = _price;
  this.name = _name;
  this.screen = _screen;
  this.backCamera = _backCamera;
  this.frontCamera = _frontCamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
}
